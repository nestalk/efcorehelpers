# EfCoreHelpers

Helper classes for working with EfCore

## DbEncryptedString

Store a string value in the database as an encrypted value.

### To use

Set the clr type of the property as `DbEncryptedString`, you can set the value by assigning a string value and can be read back out using the `ToString` method.

To set the mapping mark the property as having the convertor `DbEncryptedStringConverter`, 

```
modelBuilder.Entity<TestTable>()
    .Property(e => e.EncryptedValue)
    .HasConversion<DbEncryptedStringConverter>();
```

#### Note

You cannot compare values in a database query as the value is decrypted in memory. As a result if you want to search against the encrypted values you must first load all the values into memory by using the `AsEnumerable()` or `ToList()` methods in linq queries.