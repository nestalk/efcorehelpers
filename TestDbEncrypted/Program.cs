﻿using System.Diagnostics;
using EfCoreHelpers;
using TestDbEncrypted;

DbEncryptedString.EncryptionKey =
    "6ACvcjVkNBPT3fGLtAvY4vRDUzW2yJqdMcYP5bu3XMweLn6swJNfx65f34ZJYjqav3SjbMqtHRQ73psBFjKUNrb5ZqqNPbmdYjqbMrNUWLx58XJLD6ZYm8MHy2k6frzzzngQka3djvYUAPzZmwk5hWjptr3D82NqADnBZGFa6KEmRs45dRcmzAMnun3cR8Bp5L6B3xrEdaVUkAZvED5L5W9ck8pYZvzNThNt34eA6AZCVHVaR7Z3d7bUJRH3NcAV";

using var db = new TestContext();

for (var i = 0; i < 5; i++)
{
    var newValue = KeyGenerator.GenerateKey(10);
    var newRow = new TestTable
    {
        EncryptedValue = newValue,
        UnencryptedValue = newValue
    };
    db.Add(newRow);
}

db.SaveChanges();

var rows = db.TestEncryptions.ToList();

var stp = new Stopwatch();
stp.Start();
foreach (var row in rows)
{
    Console.WriteLine("Unencrypted: {0}", row.UnencryptedValue);
}

stp.Stop();
Console.WriteLine("Run without getting encrypted value: {0}", stp.ElapsedMilliseconds);

stp.Reset();
stp.Start();
foreach (var row in rows)
{
    Console.WriteLine("Encrypted: {0}", row.EncryptedValue);
    Console.WriteLine("Unencrypted: {0}", row.UnencryptedValue);
}

stp.Stop();
Console.WriteLine("Run getting encrypted value: {0}", stp.ElapsedMilliseconds);

var firstRow = db.TestEncryptions.First();
firstRow.EncryptedValue = $"Last run took {stp.ElapsedMilliseconds}";
// not setting the unencrypted value so it wont change
db.Update(firstRow);

var findValue = db.TestEncryptions.AsQueryable().FirstOrDefault(x => x.EncryptedValue.Equals("can you find this?"));
if (findValue is null)
{
    db.Add(new TestTable {EncryptedValue = "can you find this?", UnencryptedValue = "cant compare in the db"});
    Console.WriteLine("No it cant be compared in the db");
}

var tryFindAgain = db.TestEncryptions.AsEnumerable()
    .FirstOrDefault(x => x.EncryptedValue.Equals("ok, can you find this?"));
if (tryFindAgain is null)
{
    db.Add(new TestTable {EncryptedValue = "ok, can you find this?", UnencryptedValue = "maybe not in the db yet"});
    Console.WriteLine("Nope? maybe it hasn't been added to the db yet, try again");
}
else
{
    Console.WriteLine("Yes, can compare in memory");
}

db.SaveChanges();