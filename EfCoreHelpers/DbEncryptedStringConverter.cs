﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EfCoreHelpers;

public class DbEncryptedStringConverter : ValueConverter<DbEncryptedString, string>
{
    public DbEncryptedStringConverter() : base(
        toDb => toDb.Raw,
        fromDb => new DbEncryptedString(fromDb, true)
    )
    {
    }
}