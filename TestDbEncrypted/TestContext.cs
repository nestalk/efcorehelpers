﻿using System.ComponentModel.DataAnnotations;
using EfCoreHelpers;
using Microsoft.EntityFrameworkCore;

namespace TestDbEncrypted;

public class TestContext : DbContext
{
    public DbSet<TestTable> TestEncryptions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseSqlServer("Server=localhost;Database=TestDatabase;Trusted_Connection=True;TrustServerCertificate=True",
                options => { })
            ;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TestTable>()
            .Property(e => e.EncryptedValue)
            .IsUnicode(false)
            .HasConversion<DbEncryptedStringConverter>();

        base.OnModelCreating(modelBuilder);
    }
}

public class TestTable
{
    [Key] public int Id { get; set; }

    public DbEncryptedString EncryptedValue { get; set; }
    public string UnencryptedValue { get; set; }
}