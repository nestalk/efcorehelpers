﻿using System.Security.Cryptography;

namespace EfCoreHelpers;

public class DbEncryptedString
{
    // Key length in bytes
    private const int KeyLength = 32;

    // Salt length in bytes
    private const int SaltLength = 32;

    // IV length in bytes
    private const int IvLength = 16;

    // This constant determines the number of iterations for the password bytes generation function.
    private const int DerivationIterations = 1000;

    /// <summary>
    ///     The encryption key acts as a password (along with randomly generated salt and iteration keys) when operating on
    ///     sensitive data.
    ///     NOTE: The key must be set before you can encrypt or decrypt values
    /// </summary>
    public static string EncryptionKey = string.Empty;
    // This is the encrypted value to be stored in the database

    // Will be true if the unencrypted value is available, if false we have to decrypt the value
    private bool _isInitialized;

    // this is the unencrypted value that will be cached in memory so we dont have to decrypt a second time
    private string _unencryptedValue = string.Empty;

    /// <summary>
    ///     Creates a new instance of the DbEncrypted class
    /// </summary>
    /// <param name="value">Value of the object</param>
    /// <param name="setEncrypted">
    ///     If true you are directly setting the encrypted value.
    ///     If false you are setting the unencrypted value which will be encrypted
    /// </param>
    public DbEncryptedString(string value, bool setEncrypted = false)
    {
        if (setEncrypted)
        {
            Raw = value;
        }
        else
        {
            Value = value;
        }
    }

    // Unencrypted value
    private string Value
    {
        get
        {
            if (string.IsNullOrEmpty(EncryptionKey))
            {
                throw new InvalidOperationException("Encryption key is not set");
            }

            if (string.IsNullOrEmpty(Raw))
            {
                return string.Empty;
            }

            if (_isInitialized)
            {
                return _unencryptedValue;
            }

            _unencryptedValue = DecryptValue(Raw);
            _isInitialized = true;

            return _unencryptedValue;
        }
        init
        {
            if (string.IsNullOrEmpty(EncryptionKey))
            {
                throw new InvalidOperationException("Encryption key is not set");
            }

            Raw = EncryptValue(value);
            _unencryptedValue = value;
            _isInitialized = true;
        }
    }

    public string Raw { get; private init; } = string.Empty;

    public override int GetHashCode()
    {
        return Raw.GetHashCode();
    }

    public static implicit operator DbEncryptedString(string newValue)
    {
        return new DbEncryptedString(newValue);
    }

    public bool Equals(string otherValue)
    {
        return Value == otherValue;
    }

    public override string ToString()
    {
        return Value;
    }

    //Encryption used for sensitive data (salted)
    private static string EncryptValue(string val)
    {
        // Don't encrypt empty strings
        if (string.IsNullOrEmpty(val))
        {
            return val;
        }

        if (string.IsNullOrEmpty(EncryptionKey))
        {
            throw new InvalidOperationException("Encryption key is not set");
        }

        // Salt and IV is randomly generated each time, but is prepended to encrypted cipher text
        // so that the same Salt and IV values can be used when decrypting.  
        var saltStringBytes = Generate256BitsOfRandomEntropy();

        using var password = new Rfc2898DeriveBytes(EncryptionKey, saltStringBytes, DerivationIterations,
            HashAlgorithmName.SHA512);
        var keyBytes = password.GetBytes(KeyLength);

        using var symmetricKey = Aes.Create();
        symmetricKey.Mode = CipherMode.CBC;
        symmetricKey.Padding = PaddingMode.PKCS7;
        symmetricKey.GenerateIV();

        using var encryptor = symmetricKey.CreateEncryptor(keyBytes, symmetricKey.IV);
        using var memoryStream = new MemoryStream();
        using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        using (var streamWriter = new StreamWriter(cryptoStream))
        {
            streamWriter.Write(val);
        }

        // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
        var cipherTextBytes = saltStringBytes;
        cipherTextBytes = cipherTextBytes.Concat(symmetricKey.IV).ToArray();
        cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();

        return Convert.ToBase64String(cipherTextBytes);
    }

    private static string DecryptValue(string val)
    {
        // Empty strings cant be decrypted
        if (string.IsNullOrEmpty(val))
        {
            return val;
        }

        if (string.IsNullOrEmpty(EncryptionKey))
        {
            throw new InvalidOperationException("Encryption key is not set");
        }

        // Get the complete stream of bytes that represent:
        // [32 bytes of Salt] + [16 bytes of IV] + [n bytes of CipherText]
        var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(val).AsSpan();
        // Get the salt bytes by extracting the first 32 bytes from the supplied cipherText bytes.
        var saltStringBytes = cipherTextBytesWithSaltAndIv.Slice(0, SaltLength).ToArray();
        // Get the IV bytes by extracting the next 16 bytes from the supplied cipherText bytes.
        var ivStringBytes = cipherTextBytesWithSaltAndIv.Slice(SaltLength, IvLength).ToArray();
        // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
        var cipherTextBytes = cipherTextBytesWithSaltAndIv.Slice(SaltLength + IvLength).ToArray();

        using var password = new Rfc2898DeriveBytes(EncryptionKey, saltStringBytes.ToArray(), DerivationIterations,
            HashAlgorithmName.SHA512);
        var keyBytes = password.GetBytes(KeyLength);

        using var symmetricKey = Aes.Create();
        symmetricKey.Mode = CipherMode.CBC;
        symmetricKey.Padding = PaddingMode.PKCS7;

        using var decrypt = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes);
        using var memoryStream = new MemoryStream(cipherTextBytes);
        using var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read);
        using var streamReader = new StreamReader(cryptoStream);
        var plainText = streamReader.ReadToEnd();

        return plainText;
    }

    private static byte[] Generate256BitsOfRandomEntropy()
    {
        // 32 Bytes will give us 256 bits.
        return RandomNumberGenerator.GetBytes(32);
    }
}