﻿namespace EfCoreHelpers;

public static class KeyGenerator
{
    private static readonly char[] CharOptions =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();

    private static readonly Random Rng;

    static KeyGenerator()
    {
        Rng = new Random();
    }

    private static char GenerateChar()
    {
        var pos = Rng.Next(0, CharOptions.Length - 1);
        return CharOptions[pos];
    }

    /// <summary>
    ///     Generate a random key string
    /// </summary>
    /// <param name="length">Length of key</param>
    /// <returns>Random string key</returns>
    public static string GenerateKey(int length)
    {
        var letters = new char[length];
        for (var i = 0; i < length; i++)
        {
            letters[i] = GenerateChar();
        }

        return new string(letters);
    }
}